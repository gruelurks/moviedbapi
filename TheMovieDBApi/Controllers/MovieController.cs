﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheMovieDBApi.Helpers;

namespace TheMovieDBApi.Controllers
{
  /// <summary>Controller for the MovieDB api</summary>
  [Route("api/[controller]")]
  [ApiController]
  public class MovieController : ControllerBase
  {
    // typically these would reside in appsettings or user secrets
    private string APIKey = "b2862e2e9786d8b56a97db2274b5b26f";
    private string APIURL = "https://api.themoviedb.org/3/";
    //https://api.themoviedb.org/3/movie/upcoming?api_key=b2862e2e9786d8b56a97db2274b5b26f&language=en-US&page=1
    // GET api/values
    /// <summary>Get a collection of upcoming movies</summary>
    /// <param name="page">Current page. Defaults to 1</param>
    /// <param name="query">Search query terms.</param>
    /// <param name="language">What language to return results int. Defaults to en-US</param>
    /// <returns>JSON collection of MovieDB records. <see cref="https://developers.themoviedb.org/3/movies/get-upcoming"></see></returns>
    [HttpGet]
    [Route("UpcomingMovies")]
    public IActionResult UpcomingMoviesGet(int page = 1, string query = "", string language = "en-US")
    {
      string api = $"{APIURL}movie/upcoming?api_key={APIKey}&page={page}&query={query}&language={language}";
      string result = APIHelper.APIGet(api);
      return Ok(result);
    }

    /// <summary>Search the MovieDB</summary>
    /// <param name="page">Current page. Defaults to 1</param>
    /// <param name="query">Search query terms.</param>
    /// <param name="language">What language to return results int. Defaults to en-US</param>
    /// <returns>JSON collection of MovieDB records. <see cref="https://developers.themoviedb.org/3/search/search-movies"></see></returns>
    [HttpGet]
    [Route("SearchMovies")]
    public IActionResult SearchMovies(int page = 1, string query = "", string language = "en-US")
    {
      string api = $"{APIURL}search/movie?api_key={APIKey}&page={page}&query={query}&language={language}";
      string result = APIHelper.APIGet(api);
      return Ok(result);
    }

  }
}
