﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TheMovieDBApi.Helpers
{
  public static class APIHelper
  {
    
    
    public static string APIGet(string url)
    {
      string result = string.Empty;

      using (var client = new WebClient())
      {
        client.Headers.Add("Content-Type:application/json");
        client.Headers.Add("Accept:application/json");
        result = client.DownloadString(new Uri(url));
      }

      return result;
    }

    public static string APIPost(string url, string data)
    {
      string result = string.Empty;

      using (var client = new WebClient())
      {
        client.Headers.Add("Content-Type:application/json");
        client.Headers.Add("Accept:application/json");
        result = client.UploadString(new Uri(url), "POST", data);
      }

      return result;
    }
  }
}
